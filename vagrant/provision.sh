#!/usr/bin/env bash
#export DEBIAN_FRONTEND=noninteractive

echo "##### INFO: Started provision script"

#   add php 5.6
#add-apt-repository -y ppa:ondrej/php5-5.6

#echo "##### INFO: Added php 5.6 to system repository"

sudo aptitude update -q
sudo apt-get -y upgrade

echo "##### INFO: Updated and upgraded this system"

# Force a blank root password for mysql
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
#echo "mysql-server mysql-server/root_password password " | debconf-set-selections
#echo "mysql-server mysql-server/root_password_again password " | debconf-set-selections

echo "##### INFO: Setup some params for mysql quiet installation"

# Install mysql, nginx, php5-fpm
#sudo apt-get -y install mysql-server
sudo aptitude install -q -y -f mysql-server mysql-client nginx php5-fpm

echo "##### INFO: Installed mysql, nginx and php5-fpm"


# Install commonly used php packages
#removed php5-snmp
#sudo aptitude install -q -y -f php5-mysql php5-curl php5-gd php5-intl php-pear php5-imagick php5-imap php5-mcrypt php5-memcached php5-ming php5-ps php5-pspell php5-recode php5-sqlite php5-tidy php5-xmlrpc php5-xsl php5-xcache

#removed php5-bcmath
apt-get -y install php5 php5-mhash php5-mcrypt php5-curl php5-cli php5-mysql php5-gd php5-intl php5-xsl
apt-get -y install php-pear php5-memcached

echo "##### INFO: Installed php5 and some stuff that comes with it"

sudo apt-get -y install git

echo "##### INFO: Installed git (for automatic deployment option via edom.dev/git-deploy"

sudo rm /etc/nginx/sites-available/default
sudo touch /etc/nginx/sites-available/default


sudo cat >> /etc/nginx/sites-available/default <<'EOF'
server {
    server_name localhost;
    root /var/www/cheer/web;

    location / {
        # try to serve file directly, fallback to app.php
        try_files $uri /app.php$is_args$args;
    }
    # DEV
    # This rule should only be placed on your development environment
    # In production, don't include this and don't deploy app_dev.php or config.php
    location ~ ^/(app_dev|config)\.php(/|$) {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param  SCRIPT_FILENAME  $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
    }
    # PROD
    location ~ ^/app\.php(/|$) {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param  SCRIPT_FILENAME  $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/app.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    error_log /var/log/nginx/project_error.log;
    access_log /var/log/nginx/project_access.log;
}
EOF

echo "##### INFO: Wrote nginx site configuration"

sudo touch /usr/share/nginx/html/info.php
sudo cat >> /usr/share/nginx/html/info.php <<'EOF'
<?php phpinfo(); ?>
EOF

echo "##### INFO: Inserted phpinfo to nginx info"

# sudo aptitude install -q -y -f phpmyadmin

sudo service nginx restart

sudo service php5-fpm restart

echo "##### INFO: Restarted services"

# # set mysql password to application config
# sed -i -e "s/'password' => ''/'password' => 'root'/g" /var/www/edom/application/config/database.php
#
# nope! lets just change mysql password to NONE
mysqladmin -uroot -proot password ''

echo "##### INFO: Changed mysql root password to NONE"

#create a database
mysqladmin -uroot create symfony

echo "##### INFO: Created symfony"

# #run update schema ### doesnt work because of models, what ???
# mkdir /var/www/edom/backup_models
# mv /var/www/edom/application/models/M_* /var/www/edom/backup_models
# php /var/www/edom/application/doctrine orm:schema-tool:update --force
# mv /var/www/edom/backup_models/* /var/www/edom/application/models/
# rmdir /var/www/edom/backup_models

#php /var/www/edom/application/doctrine_schema_update.php

cd /var/www/cheer

echo "##### INFO: Changed pwd to "
pwd

# php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
# php -r "if (hash_file('SHA384', 'composer-setup.php') === '7228c001f88bee97506740ef0888240bd8a760b046ee16db8f4095c0d8d525f2367663f22a46b48d072c816e7fe19959') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
# php composer-setup.php
# php -r "unlink('composer-setup.php');"

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

echo "##### INFO: Installed composer"

composer install

echo "##### INFO: Composer install"

php bin/console doctrine:schema:update --force

echo "##### INFO: Doctrine schema update"

echo "##### INFO: ENDE"
