<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Channel
 *
 * @ORM\Table(name="channel")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ChannelRepository")
 */
class Channel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="imageurl", type="string", length=255)
     */
    private $imageurl;

    /**
     * @return mixed
     */
    public function getImageurl()
    {
        return $this->imageurl;
    }

    /**
     * @param mixed $imageurl
     */
    public function setImageurl($imageurl)
    {
        $this->imageurl = $imageurl;
    }

    /**
     * @ORM\OneToMany(targetEntity="Video", mappedBy="channel")
     */
    private $videos;

    public function __construct()
    {
        $this->videos = new ArrayCollection();
    }

    /**
     * @var datetime
     *
     * @ORM\Column(name="start", type="datetime")
     */
     private $start;

    /**
     * @return datetime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param datetime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Channel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Channel
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add video
     *
     * @param \MainBundle\Entity\Video $video
     *
     * @return Channel
     */
    public function addVideo(\MainBundle\Entity\Video $video)
    {
        $this->videos[] = $video;

        return $this;
    }

    /**
     * Remove video
     *
     * @param \MainBundle\Entity\Video $video
     */
    public function removeVideo(\MainBundle\Entity\Video $video)
    {
        $this->videos->removeElement($video);
        $video->setChannel(null);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }
}
