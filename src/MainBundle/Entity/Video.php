<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Video
 *
 * @ORM\Table(name="video")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\VideoRepository")
 */
class Video
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="yt_id", type="string", length=255)
     */
    private $ytId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="thumb", type="string", length=255)
     */
    private $thumb;

    /**
     * @ORM\ManyToOne(targetEntity="Channel", inversedBy="videos")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $channel;

    /**
     * @var dateinterval
     *
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @var isRepeat
     *
     * @ORM\Column(name="is_repeat", type="boolean")
     */
    private $isRepeat;

    /**
     * @return isRepeat
     */
    public function getIsRepeat()
    {
        return $this->isRepeat;
    }

    /**
     * @param isRepeat $isRepeat
     */
    public function setIsRepeat($isRepeat)
    {
        $this->isRepeat = $isRepeat;
    }

    /**
     * @return dateinterval
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param dateinterval $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ytId
     *
     * @param integer $ytId
     *
     * @return Video
     */
    public function setYtId($ytId)
    {
        $this->ytId = $ytId;

        return $this;
    }

    /**
     * Get ytId
     *
     * @return int
     */
    public function getYtId()
    {
        return $this->ytId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Video
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set thumb
     *
     * @param string $thumb
     *
     * @return Video
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;

        return $this;
    }

    /**
     * Get thumb
     *
     * @return string
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * Set channel
     *
     * @param \MainBundle\Entity\Channel $channel
     *
     * @return Video
     */
    public function setChannel(\MainBundle\Entity\Channel $channel = null)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel
     *
     * @return \MainBundle\Entity\Channel
     */
    public function getChannel()
    {
        return $this->channel;
    }
}
