/**
 * Created by bex on 23.4.16..
 */
$(document).ready(function(){
    $("#form_submit").addClass("btn btn-primary").css({"width": "50%", "margin-top": "30px","text-transform": "uppercase", "font-weight": "bold" });
    $(".text_widget input").css("width", "70%");
    $(".text_widget input").css("margin-bottom", "15px");
    $(".text_widget input").css("margin-top", "10px");
    $("#form_description").css({"width": "70%", "display": "block", "margin-left": "auto", "margin-right": "auto",
                                "margin-top": "15px", "height": "100px"});

    $(".plus").mouseenter(function () {
        $("#plusFaded").animate({opacity: 1});
    }).mouseout(function () {
        $("#plusFaded").animate({opacity: 0});
    })

    $(".prase").mouseenter(function () {
        $("#pigpig").animate({opacity: 1});
    }).mouseout(function () {
            $("#pigpig").animate({opacity: 0});
    })

    // $(".pocPig").click(function () {
    //     window.location = "http://127.0.0.1:8000/channels";
    // })
    
    $("#search-button").click(function () {
        $(".resultVideos").fadeOut(1).delay(1000).fadeIn(500);
    })

    $(".queueWrap").delay(500).fadeIn(500);
    

});