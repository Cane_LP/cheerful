<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MainBundle\Entity\Channel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class LandingPageController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('MainBundle:LandingPage:index.html.twig');
    }

    public function djAction($id)
    {
        $channel = $this->getDoctrine()->getRepository("MainBundle:Channel")->find($id);

        return $this->render("MainBundle:DJPage:index.html.twig", array(
            "channelName"=> $channel->getName(),
            "channelDescription"=> $channel->getDescription(),
            "channelStart" => $channel->getStart(),
            "channelImage" => $channel->getImageurl(),
            "channelId" => $channel->getId(),
            "videos" => $channel->getVideos()
        ));
    }


    private function refreshChannel($id) {
        $doc = $this->getDoctrine()->getManager();

        $channel = $doc->getRepository("MainBundle:Channel")->find($id);
        //$videos = $channel->getVideos();

        $dateNow = new \DateTime("now");
        $dateStart = clone $channel->getStart();

        $nextDateStart = $dateStart;
        $lastNextDateStart = $nextDateStart;
        $index = 0;

        while (true) {
            if (sizeof($channel->getVideos()) <= $index) {
                break;
            }

            $lastNextDateStart = clone $nextDateStart;
            $nextDateStart = $nextDateStart->add(new \DateInterval('PT'.$channel->getVideos()[$index]->getDuration().'S'));

            if ($dateNow <= $nextDateStart) {
                break;
            }

            $video = $channel->getVideos()[$index];

            if ($video->getIsRepeat()) {
                $clonedVideo = clone $video;
                $channel->addVideo($clonedVideo);
                $doc->persist($clonedVideo);
            }

            //$doc->remove($video);
            //$channel->removeVideo($video);

            $index ++;
        }

        if ($index == 0) {
            return false;
        }

        for ($i = 0; $i < $index; $i ++) {

            $video = $channel->getVideos()[$i];

            $doc->remove($video);
            $channel->removeVideo($video);
        }

        $channel->setStart($lastNextDateStart);

        $doc->persist($channel);
        $doc->flush();

        return true;
    }


    public function playerAction($id)
    {
        if ($this->refreshChannel($id)) {
            return $this->redirect($this->generateUrl('playerpage', array(
                "id" => $id
            )));
        }

        $channel = $this->getDoctrine()->getRepository("MainBundle:Channel")->find($id);
        $videos = $channel->getVideos();

        $dateNow =  new \DateTime("now");

        $dateStart = $channel->getStart();

        $dateStartNext = clone $dateStart;
        if (sizeof($videos) != 0) {
            $dateStartNext->add(new \DateInterval('PT'.$videos[0]->getDuration().'S'));
        }

        return $this->render("MainBundle:PlayerPage:index.html.twig", array(
            "channel" => $channel,
            "videos" => $videos,
            "videoTime" => $dateNow->getTimestamp() - $dateStart->getTimestamp(),
            "videoLeft" => $dateStartNext->getTimestamp() - $dateNow->getTimestamp(),
            //"videoId" => $idVideo+1
        ));
    }


}
