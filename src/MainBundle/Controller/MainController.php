<?php

namespace MainBundle\Controller;

use MainBundle\Entity\Channel;
use MainBundle\Entity\Video;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\DateTime;


class MainController extends Controller
{

    public function addChannelAction(Request $request)
    {
        $channel = new Channel();

        $name = $request->get('name');
        $description = $request->get('description');
        $imageurl = $request->get("imageurl");

        $channel->setName($name);
        $channel->setDescription($description);
        $channel->setImageurl($imageurl);
        $channel->setStart(new \DateTime("now"));

        $doc = $this->getDoctrine()->getManager();
        $doc->persist($channel);
        $doc->flush();

        return new Response($channel->getId());
    }

    public function newAction(Request $request)
    {
        $channel = new Channel();

        $form = $this->createFormBuilder($channel)
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();

        $form->handleRequest($request);
        $channel->setStart(new \DateTime("now"));
        if($form->isValid()){
            $doc = $this->getDoctrine()->getManager();
            $doc->persist($channel);
            $doc->flush();

            return $this->render('MainBundle:ChannelsPage:index.html.twig', array(
                'form' => $form->createView(),
                'message' => 'Success.'
            ));
        }

        return $this->render('MainBundle:ChannelsPage:index.html.twig', array(
            'form' => $form->createView(),
            'message' => null
        ));
    }

    public function showAllAction()
    {
        $doc = $this->getDoctrine()->getRepository("MainBundle:Channel")->findAll();

        return $this->render("MainBundle:ChannelsPage:show_all.html.twig", [
            "channels" => $doc
        ]);
    }

    public function addVideoAction(Request $request)
    {
        $video = new Video();

        $name = $request->get('name');
        $yt_id = $request->get('yt_id');
        $thumb = $request->get('thumb');
        $channelId = $request->get("channel");
        $duration = $request->get('duration');
        $repeat = $request->get('repeat');
        $repeat = ($repeat == "true");
        //var_dump($repeat);
        $channel = $this->getDoctrine()->getRepository("MainBundle:Channel")->find($channelId);

        $video->setYtId($yt_id);
        $video->setName($name);
        $video->setChannel($channel);
        $video->setThumb($thumb);
        $video->setIsRepeat($repeat);
        //var_dump($video->getIsRepeat());
        $interval = new \DateInterval($duration);
        $seconds = $interval->days*86400 + $interval->h*3600
            + $interval->i*60 + $interval->s;
        $video->setDuration($seconds);

        $doc = $this->getDoctrine()->getManager();

        //var_dump (sizeof($channel->getVideos()));
        if (sizeof($channel->getVideos()) == 0)
        {
            //var_dump("yes");
            $channel->setStart(new \Datetime("now"));
            $doc->persist($channel);
        }
//
//        $datetime = clone $channel->getStart();
//        $datetime->add(new \DateInterval('PT'.$seconds.'S'));
//        $channel->setStart($datetime);

        $doc->persist($video);
//        $doc->persist($channel);
        $doc->flush();

        return new Response();
    }
}